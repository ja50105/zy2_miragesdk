﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZY2SceneState : MonoBehaviour {
	public enum SceneState
	{
		Studio, 			//0畫室過場


		Tutorial,			//1天外音教學
		Regulate,			//2按下校正紐啟動遊戲
		IsRegulated,		//3已經校正(防止瘋狂彈跳關卡)
		WulitingInit,  		//4五里亭初始化
		Wuliting,			//5開啟扇子正在遊玩
		ParkInit, 			//6公園初始化
		Park,				//7開啟扇子正在遊玩
		SquareInit, 		//8廣場初始化
		Square,				//9廣場正在遊玩
		TSFInit,			//10龜妖蛇腰初始化
		TSF					//11龜妖蛇腰正在遊玩
	}
	// Use this for initialization
	public static ZY2SceneState Instance;
	public SceneState nowScene;

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		Instance = this;
	}

	public void UpdateState(SceneState state){
		nowScene = state;


	} 
}
