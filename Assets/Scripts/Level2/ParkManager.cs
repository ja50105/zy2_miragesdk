﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ParkManager : MonoBehaviour {
	public static ParkManager Instance;
	public Animator woman;
	public Transform hat;
	public Transform hatPos;
    public Transform head;
	// Use this for initialization

	void Awake(){
		Instance = this;
	}

	void Start () {
		hat.transform.position = hatPos.GetChild(Random.Range(0,7)).position;
        RandomPos();
	}
	
	public void Stop()
    {
        hat.transform.DOKill();
    }
	void RandomPos(){
	
        hat.transform.DOMove(hatPos.GetChild(Random.Range(0,7)).position,20 ).SetEase(Ease.InOutBounce).OnComplete(RandomPos);
	
	}
	public void MoveToHead(){
        hat.transform.DOMove(head.position, 1).SetEase(Ease.InCubic).OnComplete(()=>{
            hat.transform.parent = head;
            hat.transform.position = head.position;
            hat.transform.rotation = head.rotation;
			woman.SetTrigger("win");
        });
    }
}
