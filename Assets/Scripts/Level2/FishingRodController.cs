﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingRodController : MonoBehaviour {
	
	/// <summary>
	/// 追蹤光點
	/// </summary>
	public Transform crtlLight;
	public Transform rodSpringObj;
	/// <summary>
	/// 追蹤光點前一幀座標
	/// </summary>
	private Vector3 preCrtlPos;
	/// <summary>
	/// 位移距離
	/// </summary>
	private float crtlDeltaDis;
	/// <summary>
	/// 彈跳角度差
	/// </summary>
	private float springAngle;


	[SerializeField]
	private Transform hook;
	[SerializeField]
	private Transform hat;

	private bool follow = false;
	private bool anim = false;

	#region Properties SpinningSettings
	[Header("SpinningSettings")]
	public float power;
	[SerializeField]
	private SpinningPhysics spinningPhysics;
	[SerializeField]
	private CableComponent cableComponent;
	[SerializeField]
	private ConfigurableJoint configurableJoint;
	[SerializeField]
	private Rigidbody hookRG;
	private bool enableSwing = false;
	#endregion

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		spinningPhysics = transform.GetChild(0).GetComponent<SpinningPhysics>();
	}
	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForSeconds(0.5f);
		enableSwing = true;
	}
	
	

	// Update is called once per frame
	void Update () {
		crtlDeltaDis = Vector3.Distance(preCrtlPos, crtlLight.position);
		preCrtlPos = crtlLight.position;
		springAngle = Mathf.Abs(-90+Quaternion.Angle(transform.rotation, rodSpringObj.rotation));
		if (springAngle>80)
		{
			springAngle = Mathf.Abs(-90 + springAngle );
		}
		#if UNITY_EDITOR
			if (springAngle>10.0f && crtlDeltaDis<0.8f && enableSwing){
		#else
			if (springAngle>30.0f && crtlDeltaDis<0.1f && enableSwing){
		#endif
		
			//Debug.Log(c+"甩竿 crtlDeltaDis:"+crtlDeltaDis);
			//ZY2ControllerEvent.Instance.Log(c+"甩竿 crtlDeltaDis:"+crtlDeltaDis+"  springAngle:" + springAngle);
			cableComponent.Refreshength(1.6f);
			SoftJointLimit softJointLimit = new SoftJointLimit();
			softJointLimit.limit = 1.5f;
			configurableJoint.linearLimit = softJointLimit;
		}
		if (Vector3.Distance(hook.position, hat.position)<0.2f && follow == false && anim == false){
			StartCoroutine(NextScene());
			hat.position = hook.position;
			print("我釣到了");
			ZY2ControllerEvent.Instance.Vibrate(75,0.25f);
			ParkManager.Instance.Stop();
			follow = true;
			anim = true;
		}
		if (follow)
			hat.position = hook.position;
		
			
	}

	/// <summary>
	/// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
	/// </summary>
	void FixedUpdate()
	{
		spinningPhysics.Cast(true,power);
		
	}

	IEnumerator NextScene(){
		yield return new WaitForSeconds(2);
		follow = false;
		ParkManager.Instance.MoveToHead();
		yield return new WaitForSeconds(5f);
		hook.gameObject.SetActive(false);
		this.gameObject.SetActive(false);
		GameController.Instance.ExitPark();
	}

	public void ResetRod(){
		cableComponent.Refreshength(0.15f);
		SoftJointLimit softJointLimit = new SoftJointLimit();
		softJointLimit.limit = 0.1f;
		configurableJoint.linearLimit = softJointLimit;
	}
}
