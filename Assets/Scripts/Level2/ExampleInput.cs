﻿using Ximmerse.Vision;
using UnityEngine;
using Ximmerse.InputSystem;
using System.Collections;
using LenovoMirageARSDK;


public class ExampleInput : MonoBehaviour {

	public Transform Controller;
	private VisionSDK Sdk;
	private ControllerPeripheral controller;
	// Saber color
    private static int saberColor = (int)ColorID.BLUE;
	// Use this for initialization
	IEnumerator Start () {
		    yield return new WaitUntil(() => VisionSDK.Instance.Inited);
            Sdk = VisionSDK.Instance;
		    controller = new ControllerPeripheral("XCobra-0", Controller, null, (ColorID)saberColor);
            controller.UsePositionPrediction = true;
	}
	
	// Update is called once per frame
	void Update () {
		// Sdk.Logger.Log(controller.ControllerInput.GetRotation().ToString());
		
	}
}
