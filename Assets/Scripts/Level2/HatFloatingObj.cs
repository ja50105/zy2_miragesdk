﻿using UnityEngine;
using DG.Tweening;


public class HatFloatingObj : MonoBehaviour
{

    public Transform hatPos;

    public Transform head;

    void Awake()
    {
        transform.position = hatPos.GetChild(Random.Range(0,7)).position;
        RandomPos();
    }

    void RandomPos(){
        transform.DOMove(hatPos.GetChild(Random.Range(0,7)).position,10 ).OnComplete(RandomPos);
    }

    public void Stop()
    {
        transform.DOKill();
    }
    public void MoveToHead(){
        transform.DOMove(head.position, 1).SetEase(Ease.InCubic).OnComplete(()=>{
            this.transform.parent = head;
            this.transform.position = head.position;
            this.transform.rotation = head.rotation;
        });
    }
}