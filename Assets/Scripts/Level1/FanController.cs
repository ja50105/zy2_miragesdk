﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanController : MonoBehaviour {
	public ParticleSystem fog;

	//慣性與搖桿座標
	public Transform endObj;
	public Transform inertiaEndObj;
	private float preDistance;

	// private GameObject windParticle;
	// private ParticleSystem windParticleSystem;
	private IEnumerator coroutine;
	private bool enableWind = false;

	[System.Serializable]
	public struct Pool{
		public string tag ;
		public GameObject prefab ;
		public int size ;
	}
	public List<Pool> pools;
	public Dictionary <string,Queue<GameObject>> poolDictionary;
	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start()
	{
		poolDictionary = new Dictionary <string,Queue<GameObject>>();

		for (int i = 0; i < pools.Count; i++)
		{
			Queue<GameObject> enemyPool = new Queue<GameObject>();
			for (int j = 0; j < pools[i].size; j++)
			{
				GameObject obj = Instantiate(pools[i].prefab);
				obj.SetActive(false); 
				enemyPool.Enqueue(obj);
			}
			poolDictionary.Add(pools[i].tag, enemyPool);
		}
	}

	// Update is called once per frame
	void Update () {
		float dis = Vector3.Distance(endObj.position,inertiaEndObj.position);
		if (dis>0.75f){
			// ZY2ControllerEvent.Instance.Log(preDistance+"風兒吹啊吹"+dis);
			SpawnFromPool ("wind");
			ZY2ControllerEvent.Instance.Vibrate(75,0.1f);

			if (fog.main.maxParticles!=0 ){
				if (!enableWind){
					enableWind = true;
					coroutine = AddTornado();
					StartCoroutine(coroutine);
					var main = fog.main;
					main.maxParticles = (fog.main.maxParticles < 0) ? 0 : -5+fog.main.maxParticles; 
				}
			}
			else{
				StopCoroutine(coroutine);
				
				ClearPool("wind");
				
				this.gameObject.SetActive(false);
				Destroy(this.gameObject);

				GameController.Instance.ExitWuliting();
			}

		}
		// Debug.Log(preDistance+"    "+dis);
		preDistance = dis;

	}

	IEnumerator AddTornado(){

		yield return new WaitForSeconds(1);
		enableWind = false;
	}

	public GameObject SpawnFromPool (string tag ){
	
		if (!poolDictionary.ContainsKey(tag)){
			Debug.LogWarning("Pool with tag "+tag+ " does not excist");
			return null;
		}
		 
		GameObject objTSpwan =  poolDictionary[tag].Dequeue();
		objTSpwan.transform.position = this.transform.position;
		ParticleSystem windParticleSystem = objTSpwan.GetComponent<ParticleSystem>();
		windParticleSystem.Simulate(0.0f, true, true);
		windParticleSystem.Play();

		objTSpwan.transform.LookAt(endObj.position);
		objTSpwan.SetActive(true);


		poolDictionary[tag].Enqueue (objTSpwan);

		return objTSpwan;
	}

	public void ClearPool (string tag ){
		for (int i = 0; i < 5; i++)
		{
			Destroy(poolDictionary[tag].Dequeue());
		}
		poolDictionary[tag].Clear();
	}
}
