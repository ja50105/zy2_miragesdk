﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodParticle : MonoBehaviour,IPooledObject {
	private ParticleSystem particleSystem;
	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		particleSystem = GetComponent<ParticleSystem>();
	}
	public void OnObjectSpwan () {
		particleSystem.Simulate(0.0f, true, true);
		particleSystem.Play();
	}
}
