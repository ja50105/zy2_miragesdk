﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyStats : MonoBehaviour {
	public Stat maxHealth;			// Maximum amount of health
	public int currentHealth { get; protected set; }	// Current amount of health
	public Stat damage;
	public Stat armor;
	public UnityEvent OnHealthReachedZero;
	public event System.Action OnTakeDmgLeft;
	public event System.Action OnTakeDmgRight;
	public event System.Action OnTakeDmgFront;

    private Enemy enemy;
	
	bool isHurt;
	public virtual void Awake() {
		currentHealth = maxHealth.GetValue();
        enemy = GetComponent<Enemy>();
	}
    public void ResetHealth(){
        currentHealth = maxHealth.GetValue();
    }

	public bool TakeDamage (int damage, string Dir)
	{
		if (!isHurt)
        {
            Debug.Log(transform.name + " takes " + damage + " damage.");
            currentHealth -= damage;
            // If we hit 0. Die.
            if (currentHealth <= 0)
            {
                OnHealthReachedZero.Invoke();
                enemy.OnDeath();
            }
            else
            {            
                enemy.animator.SetTrigger(Dir);
            }
            isHurt = true;
            StartCoroutine(DelayResetHurtState());
            return isHurt;
        }
        else
            return false;
	}
	
	// Heal the character.
	public void Heal (int amount)
	{
		currentHealth += amount;
		currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth.GetValue());
	}

	IEnumerator DelayResetHurtState(){
        yield return new WaitForSeconds(1.5f);
        isHurt = false;
    }
}
