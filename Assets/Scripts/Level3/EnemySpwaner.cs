﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpwaner : MonoBehaviour {

	public static EnemySpwaner Instance;
	public Transform spwanParent;
	private EnemyPools enemyPools;
	private int count = 0;
	// Use this for initialization
	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		Instance = this;
	}
	void Start () {
		enemyPools = GetComponent<EnemyPools>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("space"))
		{
		enemyPools.SpawnFromPool("black",spwanParent.position,Quaternion.identity, spwanParent);
		}
	}
	public void SpawnFromPool(){
		if (count<9){
			enemyPools.SpawnFromPool("black",spwanParent.position+new Vector3(Random.Range(-1.5f,1.5f),0,0),Quaternion.identity, spwanParent);
			count++;
		}
	}
	public void SpawnParticleFromPool(Vector3 pos){
        enemyPools.SpawnFromPool("bloodParticle",pos,Quaternion.identity,null);
	}
}
