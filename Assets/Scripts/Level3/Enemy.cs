﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Enemy : MonoBehaviour, IPooledObject {
	public Transform target;
	public Animator animator;
	
	public enum EnemyState { Awake, DoJump ,Idle, Following, Attacking, Death };
	public EnemyState currentState;

	[Header("Move Setting")]
	[SerializeField]
    private float moveSpeed = 250;
    [SerializeField]
	private float minDistance = 50.0f;
	[SerializeField]
	private float maxDistance = 100.0f;
	[SerializeField]
    private float brakeForce = 5f;
	private float distance;


	[Header("Combat Setting")]
	[SerializeField]
	private float attackCooldown = 0f;
	[SerializeField]
	public float attackSpeed = 0.025f;
	
	[Header("Particle Setting")]
	/// <summary>
	/// 出生粒子特效
	/// </summary>
	[SerializeField]
	private GameObject bornParticle;
	/// <summary>
	/// 死亡粒子特效
	/// </summary>
	[SerializeField]
	private GameObject deathParticle;
	/// <summary>
	/// 暫存粒子特效
	/// </summary>
	private GameObject tmpParticle;
	private Rigidbody rg;
	private Image uiEffect;

	// Use this for initialization
	public void OnObjectSpwan () {
		animator.SetTrigger("Born");
		currentState = EnemyState.Awake;
		GetComponent<BoxCollider>().enabled = true;
		GetComponent<EnemyStats>().ResetHealth();
		StartCoroutine(PlayBornTween());
	}

	void Awake()
	{
		target = GameObject.FindWithTag("Player").transform;
		rg = GetComponent<Rigidbody>();
		animator = GetComponent<Animator>();
		uiEffect = GameObject.FindGameObjectWithTag("UIEffect").GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		// EnemyMovesToPlayer();
		distance = Vector3.Distance(target.transform.position, this.transform.position);
		transform.LookAt(new Vector3(target.position.x,transform.position.y,target.position.z) );
		attackCooldown -= Time.deltaTime;
		animator.SetFloat ("Speed Percent", rg.velocity.magnitude,.1f,Time.deltaTime);

        if(distance > minDistance && distance < maxDistance )
        {
			if (currentState == EnemyState.Following || currentState == EnemyState.Idle){
				currentState = EnemyState.Following;
			}
        }
		else if(distance < minDistance)
        {
			if (currentState == EnemyState.Idle || currentState != EnemyState.Attacking)
				currentState = EnemyState.Idle;
			if (attackCooldown <= 0f && currentState == EnemyState.Idle)
			{
				animator.SetTrigger ("Attack0"+ Random.Range(1,5));
				currentState = EnemyState.Attacking;
			}
		}
	}

	void FixedUpdate()
	{
		if (currentState == EnemyState.Following){
			MovesToPlayer();
		}
		else{
			StopsMoving();
		}
	}
	/// <summary>
	/// 在啟動動畫結束後 隨機播放兩種狀態
	/// </summary>
	private void RandomState(){
	
		if ( Random.Range(0, 2)==0 && distance < 7){

			transform.DOLocalMove(Vector3.back * (-1.5f+distance),1.18f).SetDelay(0.5f).OnComplete(()=>{
				currentState = EnemyState.Idle;
			});
			currentState = EnemyState.DoJump;
			animator.SetTrigger ("Jump");
		}
		else{
			currentState = EnemyState.Following;
		}

	}


	/// <summary>
	/// 移動到玩家身邊
	/// </summary>
	private void MovesToPlayer()
	{
		rg.AddRelativeForce(Vector3.forward * moveSpeed);

	}

	/// <summary>
	/// 停止移動
	/// </summary>
	private void StopsMoving()
	{ 
		rg.drag = (brakeForce);
	}
	private void OnAttackUIEffect(){
		uiEffect.DOFade(1,0.25f);
		uiEffect.DOFade(0,0.25f).SetDelay(0.25f);
	}

	/// <summary>
	/// 攻擊完成時由動畫切換狀態
	/// </summary>
	private void OnAttackComplete(){
		currentState = EnemyState.Idle;
		attackCooldown = 1f/attackSpeed;
	}

	/// <summary>
	/// 物件出生時產生出生粒子特效
	/// </summary>
	/// <returns></returns>
	IEnumerator PlayBornTween(){
		tmpParticle = Instantiate(bornParticle, this.transform, false);
		yield return new WaitForSeconds(5.8f);
		Destroy(tmpParticle);
	}

	public void OnDeath()
	{
		currentState = EnemyState.Death;
		GetComponent<BoxCollider>().enabled = false;
		animator.SetTrigger ("Death1");
		StartCoroutine("WaitDestory");
	}
	IEnumerator WaitDestory(){
		
		tmpParticle = Instantiate(deathParticle, this.transform, false);
		EnemySpwaner.Instance.SpawnFromPool();
		yield return new WaitForSeconds(5f);
		gameObject.SetActive(false);
	}
}
