﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPools : MonoBehaviour {
	// public List<GameObject> pool = new List<GameObject>();
	// public GameObject prefab;
	// [SerializeField]
	// private int spawnMaxDis = 10;
	// [SerializeField]
	// private int spawnMinDis = 7;
	// private float spawnAngle = 30;
	// private Transform beacon;
	// private Vector3 spwanPos;
	// private bool active = false;
	[System.Serializable]
	public struct Pool{
		public string tag ;
		public GameObject prefab ;
		public int size ;
	}

	public List<Pool> pools;
	public Dictionary <string,Queue<GameObject>> poolDictionary;


	#region Singleton
	public static EnemyPools Instance;
	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		Instance = this;
	}
	#endregion

	// Use this for initialization
	void Start () {

		poolDictionary = new Dictionary <string,Queue<GameObject>>();

		for (int i = 0; i < pools.Count; i++)
		{
			Queue<GameObject> enemyPool = new Queue<GameObject>();
			for (int j = 0; j < pools[i].size; j++)
			{
				GameObject obj = Instantiate(pools[i].prefab);
				obj.SetActive(false); 
				enemyPool.Enqueue(obj);
			}
			poolDictionary.Add(pools[i].tag, enemyPool);
		}
	}

	public GameObject SpawnFromPool (string tag, Vector3 pos, Quaternion rot, Transform parent ){
	
		if (!poolDictionary.ContainsKey(tag)){
			Debug.LogWarning("Pool with tag "+tag+ " does not excist");
			return null;
		}
		 
		GameObject enemyTSpwan =  poolDictionary[tag].Dequeue();
		enemyTSpwan.transform.position = pos;
		enemyTSpwan.transform.rotation = rot;
		enemyTSpwan.transform.parent = parent;
		enemyTSpwan.SetActive(true);
		
		IPooledObject poolObj = enemyTSpwan.GetComponent<IPooledObject>();

		if (poolObj != null){
			poolObj.OnObjectSpwan(); 
		}

		poolDictionary[tag].Enqueue (enemyTSpwan);

		return enemyTSpwan;
	}
}
