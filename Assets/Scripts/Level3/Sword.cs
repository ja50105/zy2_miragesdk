﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour {
	bool isHit = false;
	Vector3 prePoint;
	Vector3 nowPoint;
	public GameObject hitParticle;
	public GameObject bloodParticle;
	private EnemySpwaner enemySpwaner;
	// Use this for initialization
	void Start () {
		enemySpwaner = EnemySpwaner.Instance;
	}

	void OnTriggerStay(Collider other)
    {
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.up, out hit) )
        {
            // Debug.Log(hit.point);
			if (isHit == false){
                    nowPoint = hit.point;
                    prePoint = hit.point;
                    isHit = true;
                    return;
            }
			prePoint = nowPoint;
			nowPoint = hit.point;
			Vector3  tmp = nowPoint - prePoint;
			bool isHurt = false;
			
			if (tmp.x>0.15f){
				isHurt = other.transform.GetComponent<EnemyStats>().TakeDamage(40,"TakeDmgRight");                
            }
            else if (tmp.x<-0.15f){
				isHurt = other.transform.GetComponent<EnemyStats>().TakeDamage(40,"TakeDmgLeft");    
            }
			else{
				isHurt = other.transform.GetComponent<EnemyStats>().TakeDamage(40,"TakeDmgFront");
			}

			if(isHurt)
			{
				GameObject particle = Instantiate(hitParticle, prePoint , Quaternion.identity);
				particle.transform.LookAt(nowPoint);
				particle.transform.eulerAngles = new Vector3(particle.transform.eulerAngles.x, particle.transform.eulerAngles.y,90);
				enemySpwaner.SpawnParticleFromPool(nowPoint);
				ZY2ControllerEvent.Instance.Vibrate(100,0.3f);
			}
		}
	}
	void OnTriggerExit(Collider other)
	{
		isHit = false;
	}
}
