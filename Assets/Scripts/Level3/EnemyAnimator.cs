﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimator : MonoBehaviour {
	public Animator animator;
	// Use this for initialization
	public  void OnObjectSpwan () {
		animator.SetTrigger("Born");
	}
	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		animator = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
