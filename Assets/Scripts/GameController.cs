﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GameController : MonoBehaviour {

	/// <summary>
	/// GameController 公開靜態實例（方便其他腳本取用）
	/// </summary>
	public static GameController Instance;

	#region Public Properties

	/// <summary>
	/// 現在場景狀態
	/// </summary>
	public Animator sceneAnimator;

	#endregion

	#region Priteve Properties
	[Header("Scene State")]
	[SerializeField]
	private ZY2SceneState zy2SceneState;

	[Header("Wuliting Controller")]
	[SerializeField]
	private GameObject wulitingCrtl;


	[Header("Park Controller")]
	[SerializeField]
	private  GameObject parkCrtl ;
	[SerializeField]
	private  GameObject hook ;
	[Header("Square Controller")]
	[SerializeField]
	private  GameObject sword ;
	#endregion

	#region Unity Methods

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		zy2SceneState = ZY2SceneState.Instance;
		ZY2ControllerEvent.Instance.ControlPressUpEvent.AddListener(OnControlPressUpEvent);
		ZY2ControllerEvent.Instance.ActivatePressUpEvent.AddListener(OnActivatePressUp);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	#endregion

	#region Pritave Methods
	
	/// <summary>
	/// 校正按鈕放開事件
	/// </summary>
	private void OnControlPressUpEvent(){
		if ( zy2SceneState.nowScene == ZY2SceneState.SceneState.Regulate){
			InitScene();
		}
	}

	private void OnActivatePressUp(){
		//五里亭控制器開啟
		if ( zy2SceneState.nowScene == ZY2SceneState.SceneState.WulitingInit){
			wulitingCrtl.SetActive(true);
			zy2SceneState.nowScene = ZY2SceneState.SceneState.Wuliting;
			// Time.timeScale = 1;
		}
		//公園控制器開啟
		else if ( zy2SceneState.nowScene == ZY2SceneState.SceneState.ParkInit){
			parkCrtl.SetActive(true);
			hook.transform.parent = null;
			zy2SceneState.nowScene = ZY2SceneState.SceneState.Park;
			// Time.timeScale = 1;
		}
		//廣場控制器開啟
		else if ( zy2SceneState.nowScene == ZY2SceneState.SceneState.SquareInit){
			sword.SetActive(true);
			zy2SceneState.nowScene = ZY2SceneState.SceneState.Square;
			EnemySpwaner.Instance.SpawnFromPool();
			// Time.timeScale = 1;
		}
	}

	private void InitScene(){
		sceneAnimator.SetTrigger("next");
		zy2SceneState.nowScene = ZY2SceneState.SceneState.IsRegulated;
	}

	public void ExitWuliting(){
		sceneAnimator.SetTrigger("next");
		zy2SceneState.nowScene = ZY2SceneState.SceneState.Studio;
		// Time.timeScale = 10;
	}

	public void ExitPark(){
		sceneAnimator.SetTrigger("next");
		zy2SceneState.nowScene = ZY2SceneState.SceneState.Studio;
		// Time.timeScale = 10;
	}


	#endregion
}
