﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LenovoMirageARSDK;
using Ximmerse.Vision;
using UnityEngine.Events;

using DeviceManager;


public class ZY2ControllerEvent : MonoBehaviour {
	public static ZY2ControllerEvent Instance;

	public UnityEvent ActivatePressUpEvent;
	public UnityEvent ControlPressUpEvent;
	private GameController gameController;
	
	
	#region Private Properties MirageAR

	//以下是聯想SDK所需參數
	private VisionSDK Sdk;
	private MirageAR_SDK mirageAR_SDK;
	private MirageAR_InputListener mInputListener;
	private ControllerPeripheral controller;

	private float audioStep = 1.0f/15;
	private float audioValue;
	private bool Inited = false;
	#endregion


	#region Unity Methods
	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	IEnumerator Start () {
		gameController = GameController.Instance;
		
		yield return new WaitUntil(() => VisionSDK.Instance.Inited);
		Inited = true;
		Sdk = VisionSDK.Instance;
		mirageAR_SDK = MirageAR_SDK.Instance;
        mInputListener = MirageAR_InputListener.Instance;
		mInputListener.SaberControlPressUp +=OnControlPressUp;
		mInputListener.SaberActivatePressUp += OnActivatePressUp;

		controller = mirageAR_SDK.ControllerPeripheral;
    	controller.UsePositionPrediction = true;
		
		
	
		mInputListener.HmdBackPressUp += HmdBackPressUp;
		mInputListener.HmdMenuPressUp += HmdMenuPressUp;



		//InvokeRepeating("ChangeColor",0,0.5f);
		Sdk.Tracking.Recenter(true);

	}

	#if UNITY_EDITOR
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyUp("o"))
        {
			OnControlPressUp();
        }
		if (Input.GetKeyUp("p"))
        {
			OnActivatePressUp();
        }
	
	}
	#endif
	
	#endregion


	#region MirageAR Methods

	public void OnControlPressUp(){
		mirageAR_SDK.ControllerPeripheral.Recenter(Sdk.StereoCamera);
		Sdk.Tracking.Recenter();
		ControlPressUpEvent.Invoke();
	}

	public void OnActivatePressUp()
	{
		mirageAR_SDK.ControllerPeripheral.Recenter(Sdk.StereoCamera);
		ActivatePressUpEvent.Invoke();	
	}
	
	public void HmdMenuPressUp(){
		audioValue = DeviceUtils.GetVolume();
		audioValue = Mathf.Clamp(audioValue + audioStep, 0, 15);
        DeviceUtils.SetVolume(audioValue);
	}

	public void HmdBackPressUp(){
		audioValue = DeviceUtils.GetVolume();
		audioValue = Mathf.Clamp(audioValue - audioStep, 0, 15);
		DeviceUtils.SetVolume(audioValue);
	}

	public void Log( string a ){
		Sdk.Logger.Log(a);
	}

	public void Vibrate(int strength,float duration)
	{
		mirageAR_SDK.ControllerPeripheral.Vibrate(strength,duration);
	}

	#endregion
}
