// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:34664,y:32800,varname:node_3138,prsc:2|emission-5079-OUT,alpha-6867-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:34013,y:32776,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Tex2dAsset,id:736,x:32141,y:32845,ptovrint:False,ptlb:Flowmap,ptin:_Flowmap,varname:node_736,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Panner,id:713,x:32141,y:33044,varname:node_713,prsc:2,spu:1,spv:0|UVIN-5047-UVOUT,DIST-3675-OUT;n:type:ShaderForge.SFN_Panner,id:6466,x:32141,y:33202,varname:node_6466,prsc:2,spu:0,spv:1|UVIN-5047-UVOUT,DIST-2129-OUT;n:type:ShaderForge.SFN_Tex2d,id:4488,x:32531,y:32954,varname:node_4488,prsc:2,ntxv:0,isnm:False|UVIN-713-UVOUT,TEX-736-TEX;n:type:ShaderForge.SFN_Tex2d,id:4323,x:32531,y:33106,varname:node_4323,prsc:2,ntxv:0,isnm:False|UVIN-6466-UVOUT,TEX-736-TEX;n:type:ShaderForge.SFN_TexCoord,id:5047,x:31660,y:32925,varname:node_5047,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:3675,x:31866,y:33170,varname:node_3675,prsc:2|A-619-T,B-875-OUT;n:type:ShaderForge.SFN_Time,id:619,x:31660,y:33148,varname:node_619,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:875,x:31660,y:33310,ptovrint:False,ptlb:U_flowspeed,ptin:_U_flowspeed,varname:node_875,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:2182,x:31660,y:33393,ptovrint:False,ptlb:v_flowspeed,ptin:_v_flowspeed,varname:node_2182,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.25;n:type:ShaderForge.SFN_Multiply,id:2129,x:31866,y:33297,varname:node_2129,prsc:2|A-619-T,B-2182-OUT;n:type:ShaderForge.SFN_Append,id:6496,x:32784,y:32954,varname:node_6496,prsc:2|A-4488-R,B-4323-G;n:type:ShaderForge.SFN_Multiply,id:2116,x:32999,y:32954,varname:node_2116,prsc:2|A-6496-OUT,B-9092-OUT,C-6418-V;n:type:ShaderForge.SFN_ValueProperty,id:9092,x:32832,y:33173,ptovrint:False,ptlb:flowstrengh,ptin:_flowstrengh,varname:node_9092,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Add,id:1258,x:33172,y:32926,varname:node_1258,prsc:2|A-5047-UVOUT,B-2116-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:4481,x:33172,y:32744,ptovrint:False,ptlb:main tex,ptin:_maintex,varname:node_4481,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5274,x:33616,y:32926,varname:node_5274,prsc:2,ntxv:0,isnm:False|UVIN-4927-UVOUT,TEX-4481-TEX;n:type:ShaderForge.SFN_Panner,id:4927,x:33392,y:32926,varname:node_4927,prsc:2,spu:-0.05,spv:0|UVIN-1258-OUT,DIST-6418-U;n:type:ShaderForge.SFN_Tex2d,id:1559,x:33616,y:33135,varname:node_1559,prsc:2,ntxv:0,isnm:False|UVIN-8218-UVOUT,TEX-4481-TEX;n:type:ShaderForge.SFN_Panner,id:8218,x:33392,y:33135,varname:node_8218,prsc:2,spu:-1,spv:0|UVIN-5047-UVOUT,DIST-6418-U;n:type:ShaderForge.SFN_Multiply,id:6639,x:33827,y:32948,varname:node_6639,prsc:2|A-5274-R,B-1559-G;n:type:ShaderForge.SFN_Multiply,id:5079,x:34185,y:32930,varname:node_5079,prsc:2|A-7241-RGB,B-6639-OUT,C-1414-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1414,x:34013,y:33048,ptovrint:False,ptlb:emissStrengh,ptin:_emissStrengh,varname:node_1414,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Fresnel,id:8617,x:33504,y:33457,varname:node_8617,prsc:2|EXP-504-OUT;n:type:ShaderForge.SFN_ValueProperty,id:504,x:33329,y:33477,ptovrint:False,ptlb:fresnelstrengh,ptin:_fresnelstrengh,varname:node_504,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:4;n:type:ShaderForge.SFN_OneMinus,id:9954,x:33678,y:33457,varname:node_9954,prsc:2|IN-8617-OUT;n:type:ShaderForge.SFN_Multiply,id:4716,x:33862,y:33457,varname:node_4716,prsc:2|A-9954-OUT,B-9954-OUT,C-4331-OUT;n:type:ShaderForge.SFN_Vector1,id:4331,x:33678,y:33586,varname:node_4331,prsc:2,v1:2;n:type:ShaderForge.SFN_Clamp01,id:7450,x:34024,y:33457,varname:node_7450,prsc:2|IN-4716-OUT;n:type:ShaderForge.SFN_Multiply,id:6867,x:34254,y:33135,varname:node_6867,prsc:2|A-7134-OUT,B-7450-OUT,C-1307-OUT;n:type:ShaderForge.SFN_Tex2d,id:9880,x:33656,y:33642,varname:node_9880,prsc:2,ntxv:0,isnm:False|UVIN-1616-OUT,TEX-4481-TEX;n:type:ShaderForge.SFN_Set,id:3012,x:31868,y:32760,varname:UV,prsc:2|IN-5047-UVOUT;n:type:ShaderForge.SFN_Get,id:1616,x:33441,y:33642,varname:node_1616,prsc:2|IN-3012-OUT;n:type:ShaderForge.SFN_Add,id:7520,x:33862,y:33642,varname:node_7520,prsc:2|A-9880-B,B-6418-Z;n:type:ShaderForge.SFN_Clamp01,id:1307,x:34024,y:33642,varname:node_1307,prsc:2|IN-7520-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8785,x:33809,y:33169,ptovrint:False,ptlb:alphastrengh,ptin:_alphastrengh,varname:node_8785,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:7134,x:34062,y:33135,varname:node_7134,prsc:2|A-6639-OUT,B-8785-OUT;n:type:ShaderForge.SFN_TexCoord,id:6418,x:33044,y:33307,cmnt:U.BrushOffset_V.FlowStrengh_Z.FadeOut,varname:node_6418,prsc:2,uv:1,uaff:True;proporder:7241-736-875-2182-9092-4481-1414-504-8785;pass:END;sub:END;*/

Shader "Shader Forge/ink brush" {
    Properties {
        _Color ("Color", Color) = (0,0,0,1)
        _Flowmap ("Flowmap", 2D) = "white" {}
        _U_flowspeed ("U_flowspeed", Float ) = 0.5
        _v_flowspeed ("v_flowspeed", Float ) = 0.25
        _flowstrengh ("flowstrengh", Float ) = 0.1
        _maintex ("main tex", 2D) = "white" {}
        _emissStrengh ("emissStrengh", Float ) = 1
        _fresnelstrengh ("fresnelstrengh", Float ) = 4
        _alphastrengh ("alphastrengh", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _Flowmap; uniform float4 _Flowmap_ST;
            uniform float _U_flowspeed;
            uniform float _v_flowspeed;
            uniform float _flowstrengh;
            uniform sampler2D _maintex; uniform float4 _maintex_ST;
            uniform float _emissStrengh;
            uniform float _fresnelstrengh;
            uniform float _alphastrengh;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_619 = _Time + _TimeEditor;
                float2 node_713 = (i.uv0+(node_619.g*_U_flowspeed)*float2(1,0));
                float4 node_4488 = tex2D(_Flowmap,TRANSFORM_TEX(node_713, _Flowmap));
                float2 node_6466 = (i.uv0+(node_619.g*_v_flowspeed)*float2(0,1));
                float4 node_4323 = tex2D(_Flowmap,TRANSFORM_TEX(node_6466, _Flowmap));
                float2 node_4927 = ((i.uv0+(float2(node_4488.r,node_4323.g)*_flowstrengh*i.uv1.g))+i.uv1.r*float2(-0.05,0));
                float4 node_5274 = tex2D(_maintex,TRANSFORM_TEX(node_4927, _maintex));
                float2 node_8218 = (i.uv0+i.uv1.r*float2(-1,0));
                float4 node_1559 = tex2D(_maintex,TRANSFORM_TEX(node_8218, _maintex));
                float node_6639 = (node_5274.r*node_1559.g);
                float3 emissive = (_Color.rgb*node_6639*_emissStrengh);
                float3 finalColor = emissive;
                float node_9954 = (1.0 - pow(1.0-max(0,dot(normalDirection, viewDirection)),_fresnelstrengh));
                float2 UV = i.uv0;
                float2 node_1616 = UV;
                float4 node_9880 = tex2D(_maintex,TRANSFORM_TEX(node_1616, _maintex));
                return fixed4(finalColor,((node_6639*_alphastrengh)*saturate((node_9954*node_9954*2.0))*saturate((node_9880.b+i.uv1.b))));
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
