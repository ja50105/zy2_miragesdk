// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33275,y:32249,varname:node_3138,prsc:2|emission-2084-OUT,alpha-9286-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32447,y:32254,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2dAsset,id:9886,x:32222,y:32247,ptovrint:False,ptlb:MIAN TEX,ptin:_MIANTEX,varname:node_9886,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2dAsset,id:5915,x:31417,y:32324,ptovrint:False,ptlb:fLOWMAP,ptin:_fLOWMAP,varname:node_5915,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:7081,x:31627,y:32656,varname:node_7081,prsc:2,ntxv:0,isnm:False|UVIN-7075-UVOUT,TEX-5915-TEX;n:type:ShaderForge.SFN_Tex2d,id:6761,x:31629,y:32916,varname:node_6761,prsc:2,ntxv:0,isnm:False|UVIN-4314-UVOUT,TEX-5915-TEX;n:type:ShaderForge.SFN_Panner,id:7075,x:31428,y:32656,varname:node_7075,prsc:2,spu:0.5,spv:0|UVIN-2790-UVOUT,DIST-4196-OUT;n:type:ShaderForge.SFN_Panner,id:4314,x:31431,y:32916,varname:node_4314,prsc:2,spu:0,spv:-0.5|UVIN-2790-UVOUT,DIST-4484-OUT;n:type:ShaderForge.SFN_TexCoord,id:2790,x:31185,y:32516,varname:node_2790,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:4196,x:31163,y:32932,varname:node_4196,prsc:2|A-7512-T,B-3042-OUT;n:type:ShaderForge.SFN_Time,id:7512,x:30891,y:32928,varname:node_7512,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:3042,x:30891,y:33077,ptovrint:False,ptlb:U_flowSpeed,ptin:_U_flowSpeed,varname:node_3042,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_ValueProperty,id:1137,x:30891,y:33153,ptovrint:False,ptlb:V_flowspeed,ptin:_V_flowspeed,varname:node_1137,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_Multiply,id:4484,x:31163,y:33102,varname:node_4484,prsc:2|A-7512-T,B-1137-OUT;n:type:ShaderForge.SFN_Append,id:9151,x:31832,y:32656,varname:node_9151,prsc:2|A-7081-R,B-6761-G;n:type:ShaderForge.SFN_Multiply,id:4363,x:32032,y:32656,varname:node_4363,prsc:2|A-9151-OUT,B-4578-OUT,C-1051-U;n:type:ShaderForge.SFN_ValueProperty,id:4578,x:31832,y:32833,ptovrint:False,ptlb:FlowStrengh,ptin:_FlowStrengh,varname:node_4578,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.25;n:type:ShaderForge.SFN_Add,id:7995,x:32217,y:32521,varname:node_7995,prsc:2|A-2790-UVOUT,B-4363-OUT;n:type:ShaderForge.SFN_Tex2d,id:63,x:32447,y:32521,varname:node_63,prsc:2,ntxv:0,isnm:False|UVIN-7995-OUT,TEX-9886-TEX;n:type:ShaderForge.SFN_Multiply,id:6046,x:32752,y:32273,varname:node_6046,prsc:2|A-7241-RGB,B-63-R,C-1870-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1870,x:32447,y:32457,ptovrint:False,ptlb:EmissStrengh,ptin:_EmissStrengh,varname:node_1870,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:4651,x:32756,y:32644,ptovrint:False,ptlb:AlphaStrengh,ptin:_AlphaStrengh,varname:node_4651,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_VertexColor,id:9670,x:32756,y:32429,varname:node_9670,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2084,x:32974,y:32294,varname:node_2084,prsc:2|A-6046-OUT,B-9670-RGB;n:type:ShaderForge.SFN_Multiply,id:9286,x:32970,y:32560,varname:node_9286,prsc:2|A-63-R,B-9670-A,C-4651-OUT;n:type:ShaderForge.SFN_TexCoord,id:1051,x:31832,y:32916,varname:node_1051,prsc:2,uv:1,uaff:True;proporder:7241-9886-5915-3042-1137-4578-1870-4651;pass:END;sub:END;*/

Shader "Shader Forge/ink" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MIANTEX ("MIAN TEX", 2D) = "white" {}
        _fLOWMAP ("fLOWMAP", 2D) = "white" {}
        _U_flowSpeed ("U_flowSpeed", Float ) = 0.2
        _V_flowspeed ("V_flowspeed", Float ) = 0.2
        _FlowStrengh ("FlowStrengh", Float ) = 0.25
        _EmissStrengh ("EmissStrengh", Float ) = 0
        _AlphaStrengh ("AlphaStrengh", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _MIANTEX; uniform float4 _MIANTEX_ST;
            uniform sampler2D _fLOWMAP; uniform float4 _fLOWMAP_ST;
            uniform float _U_flowSpeed;
            uniform float _V_flowspeed;
            uniform float _FlowStrengh;
            uniform float _EmissStrengh;
            uniform float _AlphaStrengh;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 uv1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 node_7512 = _Time + _TimeEditor;
                float2 node_7075 = (i.uv0+(node_7512.g*_U_flowSpeed)*float2(0.5,0));
                float4 node_7081 = tex2D(_fLOWMAP,TRANSFORM_TEX(node_7075, _fLOWMAP));
                float2 node_4314 = (i.uv0+(node_7512.g*_V_flowspeed)*float2(0,-0.5));
                float4 node_6761 = tex2D(_fLOWMAP,TRANSFORM_TEX(node_4314, _fLOWMAP));
                float2 node_7995 = (i.uv0+(float2(node_7081.r,node_6761.g)*_FlowStrengh*i.uv1.r));
                float4 node_63 = tex2D(_MIANTEX,TRANSFORM_TEX(node_7995, _MIANTEX));
                float3 emissive = ((_Color.rgb*node_63.r*_EmissStrengh)*i.vertexColor.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,(node_63.r*i.vertexColor.a*_AlphaStrengh));
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
