﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DissolvePos : MonoBehaviour {
	/// <summary>
	/// 只能使用自訂的 dissolve 材質球
	/// </summary>
	/// <typeparam name="Material"></typeparam>
	/// <returns></returns>
	public List<Material> dissolve = new List<Material>();
	public float radius = 0;
	public float disLineWidth = 0;
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < dissolve.Count; i++)
		{
			dissolve[i].SetFloat("_Radius",radius);
			dissolve[i].SetVector("_Position",transform.position);
			dissolve[i].SetFloat("_DisLineWidth",disLineWidth);
		}
	}
	void OnDrawGizmosSelected() {
        Gizmos.color = new Color(1, 0, 0, 1F);
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
