// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:1,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:34616,y:32844,varname:node_3138,prsc:2|diff-6402-RGB,emission-5811-OUT,transm-1171-OUT,lwrap-3578-RGB,clip-9169-OUT,olwid-4726-OUT,olcol-9785-RGB;n:type:ShaderForge.SFN_Tex2dAsset,id:6004,x:33728,y:32542,ptovrint:False,ptlb:ramp,ptin:_ramp,varname:node_6004,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4907,x:33967,y:32395,varname:node_4907,prsc:2,ntxv:0,isnm:False|UVIN-1269-OUT,TEX-6004-TEX;n:type:ShaderForge.SFN_Append,id:1269,x:33728,y:32312,varname:node_1269,prsc:2|A-7443-OUT,B-7599-OUT;n:type:ShaderForge.SFN_Tex2d,id:86,x:33053,y:33262,ptovrint:False,ptlb:dissovle map,ptin:_dissovlemap,varname:node_86,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Slider,id:7823,x:32451,y:32967,ptovrint:False,ptlb:dissovle amount,ptin:_dissovleamount,varname:node_7823,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.0341883,max:1;n:type:ShaderForge.SFN_Add,id:9169,x:33379,y:33029,varname:node_9169,prsc:2|A-9215-OUT,B-86-R;n:type:ShaderForge.SFN_RemapRange,id:9215,x:33060,y:32999,varname:node_9215,prsc:2,frmn:0.5,frmx:1,tomn:-0.5,tomx:0.5|IN-7249-OUT;n:type:ShaderForge.SFN_OneMinus,id:7249,x:32893,y:33067,varname:node_7249,prsc:2|IN-7823-OUT;n:type:ShaderForge.SFN_RemapRange,id:2211,x:33184,y:32297,varname:node_2211,prsc:2,frmn:0,frmx:1,tomn:-4,tomx:4;n:type:ShaderForge.SFN_Clamp01,id:7019,x:33374,y:32297,varname:node_7019,prsc:2|IN-2211-OUT;n:type:ShaderForge.SFN_OneMinus,id:7443,x:33543,y:32297,varname:node_7443,prsc:2|IN-7019-OUT;n:type:ShaderForge.SFN_Tex2d,id:6402,x:34263,y:32669,ptovrint:False,ptlb:texture,ptin:_texture,varname:node_6402,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:4726,x:33994,y:33247,ptovrint:False,ptlb:outline width,ptin:_outlinewidth,varname:node_4726,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.008547009,max:1;n:type:ShaderForge.SFN_Color,id:9785,x:33994,y:33352,ptovrint:False,ptlb:outline color,ptin:_outlinecolor,varname:node_9785,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Fresnel,id:1171,x:34450,y:33467,varname:node_1171,prsc:2|EXP-264-OUT;n:type:ShaderForge.SFN_Slider,id:264,x:34108,y:33528,ptovrint:False,ptlb:transmission,ptin:_transmission,varname:node_264,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Color,id:3578,x:34368,y:33024,ptovrint:False,ptlb:light wrapping,ptin:_lightwrapping,varname:node_3578,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Fresnel,id:5811,x:33800,y:32871,varname:node_5811,prsc:2|EXP-4830-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4830,x:33131,y:32793,ptovrint:False,ptlb:rim,ptin:_rim,varname:node_4830,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Vector1,id:7599,x:33430,y:32503,varname:node_7599,prsc:2,v1:0;proporder:6402-6004-86-7823-4726-9785-264-3578-4830;pass:END;sub:END;*/

Shader "Shader Forge/road appears" {
    Properties {
        _texture ("texture", 2D) = "white" {}
        _ramp ("ramp", 2D) = "white" {}
        _dissovlemap ("dissovle map", 2D) = "black" {}
        _dissovleamount ("dissovle amount", Range(0, 1)) = 0.0341883
        _outlinewidth ("outline width", Range(0, 1)) = 0.008547009
        _outlinecolor ("outline color", Color) = (0.5,0.5,0.5,1)
        _transmission ("transmission", Range(0, 1)) = 0
        _lightwrapping ("light wrapping", Color) = (1,1,1,1)
        _rim ("rim", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform sampler2D _dissovlemap; uniform float4 _dissovlemap_ST;
            uniform float _dissovleamount;
            uniform float _outlinewidth;
            uniform float4 _outlinecolor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( float4(v.vertex.xyz + v.normal*_outlinewidth,1) );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 _dissovlemap_var = tex2D(_dissovlemap,TRANSFORM_TEX(i.uv0, _dissovlemap));
                clip((((1.0 - _dissovleamount)*2.0+-1.5)+_dissovlemap_var.r) - 0.5);
                return fixed4(_outlinecolor.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform float4 _LightColor0;
            uniform sampler2D _dissovlemap; uniform float4 _dissovlemap_ST;
            uniform float _dissovleamount;
            uniform sampler2D _texture; uniform float4 _texture_ST;
            uniform float _transmission;
            uniform float4 _lightwrapping;
            uniform float _rim;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _dissovlemap_var = tex2D(_dissovlemap,TRANSFORM_TEX(i.uv0, _dissovlemap));
                clip((((1.0 - _dissovleamount)*2.0+-1.5)+_dissovlemap_var.r) - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 w = _lightwrapping.rgb*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float node_1171 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_transmission);
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(node_1171,node_1171,node_1171);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = (forwardLight+backLight) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 _texture_var = tex2D(_texture,TRANSFORM_TEX(i.uv0, _texture));
                float3 diffuseColor = _texture_var.rgb;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float node_5811 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_rim);
                float3 emissive = float3(node_5811,node_5811,node_5811);
/// Final Color:
                float3 finalColor = diffuse + emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform float4 _LightColor0;
            uniform sampler2D _dissovlemap; uniform float4 _dissovlemap_ST;
            uniform float _dissovleamount;
            uniform sampler2D _texture; uniform float4 _texture_ST;
            uniform float _transmission;
            uniform float4 _lightwrapping;
            uniform float _rim;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _dissovlemap_var = tex2D(_dissovlemap,TRANSFORM_TEX(i.uv0, _dissovlemap));
                clip((((1.0 - _dissovleamount)*2.0+-1.5)+_dissovlemap_var.r) - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 w = _lightwrapping.rgb*0.5; // Light wrapping
                float3 NdotLWrap = NdotL * ( 1.0 - w );
                float3 forwardLight = max(float3(0.0,0.0,0.0), NdotLWrap + w );
                float node_1171 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_transmission);
                float3 backLight = max(float3(0.0,0.0,0.0), -NdotLWrap + w ) * float3(node_1171,node_1171,node_1171);
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = (forwardLight+backLight) * attenColor;
                float4 _texture_var = tex2D(_texture,TRANSFORM_TEX(i.uv0, _texture));
                float3 diffuseColor = _texture_var.rgb;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform sampler2D _dissovlemap; uniform float4 _dissovlemap_ST;
            uniform float _dissovleamount;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 _dissovlemap_var = tex2D(_dissovlemap,TRANSFORM_TEX(i.uv0, _dissovlemap));
                clip((((1.0 - _dissovleamount)*2.0+-1.5)+_dissovlemap_var.r) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
