// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:9361,x:33319,y:32705,varname:node_9361,prsc:2|emission-8269-OUT,custl-9532-OUT,voffset-5506-OUT;n:type:ShaderForge.SFN_Tex2d,id:6792,x:32673,y:33206,ptovrint:False,ptlb:flow map,ptin:_flowmap,varname:node_6792,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-7106-OUT;n:type:ShaderForge.SFN_Panner,id:5725,x:32374,y:33206,varname:node_5725,prsc:2,spu:1,spv:1|UVIN-9777-UVOUT,DIST-2228-OUT;n:type:ShaderForge.SFN_Time,id:9659,x:31741,y:32718,varname:node_9659,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:9777,x:31741,y:32547,varname:node_9777,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:2228,x:32184,y:33206,varname:node_2228,prsc:2|A-9659-T,B-1163-OUT;n:type:ShaderForge.SFN_Slider,id:1163,x:31842,y:33363,ptovrint:False,ptlb:flow speed,ptin:_flowspeed,varname:node_1163,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.1;n:type:ShaderForge.SFN_RemapRange,id:7106,x:32525,y:33206,varname:node_7106,prsc:2,frmn:1,frmx:-1,tomn:1,tomx:-1|IN-5725-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:825,x:32486,y:33014,ptovrint:False,ptlb:texture,ptin:_texture,varname:node_825,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:7936ef474f4d2244b8ae0e25c5dca344,ntxv:0,isnm:False;n:type:ShaderForge.SFN_NormalVector,id:5464,x:32673,y:33364,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:5506,x:32922,y:33206,varname:node_5506,prsc:2|A-6792-RGB,B-5464-OUT,C-8988-OUT;n:type:ShaderForge.SFN_Slider,id:8988,x:32516,y:33526,ptovrint:False,ptlb:flow strengh,ptin:_flowstrengh,varname:node_8988,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.1;n:type:ShaderForge.SFN_Multiply,id:9532,x:32870,y:32953,varname:node_9532,prsc:2|A-6269-RGB,B-825-RGB;n:type:ShaderForge.SFN_Color,id:6269,x:32486,y:32852,ptovrint:False,ptlb:main ccolor,ptin:_mainccolor,varname:node_6269,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_ComponentMask,id:164,x:32379,y:32576,varname:node_164,prsc:2,cc1:0,cc2:1,cc3:2,cc4:-1|IN-9682-RGB;n:type:ShaderForge.SFN_Tex2d,id:9682,x:32191,y:32576,ptovrint:False,ptlb:mask texture,ptin:_masktexture,varname:node_9682,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:e91b61bbb1d59504f9f9c5e5405618ff,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:8269,x:33100,y:32616,varname:node_8269,prsc:2|A-830-OUT,B-7101-OUT;n:type:ShaderForge.SFN_Tex2d,id:4835,x:32523,y:32218,varname:node_4835,prsc:2,ntxv:0,isnm:False|UVIN-2124-UVOUT,TEX-598-TEX;n:type:ShaderForge.SFN_Slider,id:1241,x:32553,y:32754,ptovrint:False,ptlb:rim strengh,ptin:_rimstrengh,varname:node_1241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3964849,max:1;n:type:ShaderForge.SFN_Multiply,id:7101,x:32898,y:32645,varname:node_7101,prsc:2|A-164-OUT,B-1241-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3504,x:31741,y:32448,ptovrint:False,ptlb:rim flow speed,ptin:_rimflowspeed,varname:node_3504,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:5527,x:31995,y:32228,varname:node_5527,prsc:2|A-3504-OUT,B-9659-T;n:type:ShaderForge.SFN_Multiply,id:3045,x:32020,y:32417,varname:node_3045,prsc:2|A-3504-OUT,B-9659-T;n:type:ShaderForge.SFN_Panner,id:2124,x:32201,y:32228,varname:node_2124,prsc:2,spu:0,spv:0.2|UVIN-9777-UVOUT,DIST-5527-OUT;n:type:ShaderForge.SFN_Panner,id:8681,x:32211,y:32417,varname:node_8681,prsc:2,spu:0.2,spv:0|UVIN-9777-UVOUT,DIST-3045-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:598,x:32347,y:32211,ptovrint:False,ptlb:rim texture,ptin:_rimtexture,varname:node_598,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4867,x:32509,y:32391,varname:node_4867,prsc:2,ntxv:0,isnm:False|UVIN-8681-UVOUT,TEX-598-TEX;n:type:ShaderForge.SFN_Multiply,id:1622,x:32706,y:32391,varname:node_1622,prsc:2|A-4835-RGB,B-4867-RGB;n:type:ShaderForge.SFN_Color,id:5612,x:32706,y:32218,ptovrint:False,ptlb:rim color,ptin:_rimcolor,varname:node_5612,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:830,x:32988,y:32349,varname:node_830,prsc:2|A-5612-RGB,B-1622-OUT;proporder:825-6269-9682-6792-8988-1163-598-5612-1241-3504;pass:END;sub:END;*/

Shader "Shader Forge/water 02" {
    Properties {
        _texture ("texture", 2D) = "white" {}
        _mainccolor ("main ccolor", Color) = (0.5,0.5,0.5,1)
        _masktexture ("mask texture", 2D) = "white" {}
        _flowmap ("flow map", 2D) = "white" {}
        _flowstrengh ("flow strengh", Range(0, 0.1)) = 0
        _flowspeed ("flow speed", Range(0, 0.1)) = 0
        _rimtexture ("rim texture", 2D) = "white" {}
        _rimcolor ("rim color", Color) = (0.5,0.5,0.5,1)
        _rimstrengh ("rim strengh", Range(0, 1)) = 0.3964849
        _rimflowspeed ("rim flow speed", Float ) = 0.5
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform float4 _TimeEditor;
            uniform sampler2D _flowmap; uniform float4 _flowmap_ST;
            uniform float _flowspeed;
            uniform sampler2D _texture; uniform float4 _texture_ST;
            uniform float _flowstrengh;
            uniform float4 _mainccolor;
            uniform sampler2D _masktexture; uniform float4 _masktexture_ST;
            uniform float _rimstrengh;
            uniform float _rimflowspeed;
            uniform sampler2D _rimtexture; uniform float4 _rimtexture_ST;
            uniform float4 _rimcolor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_9659 = _Time + _TimeEditor;
                float2 node_7106 = ((o.uv0+(node_9659.g*_flowspeed)*float2(1,1))*1.0+0.0);
                float4 _flowmap_var = tex2Dlod(_flowmap,float4(TRANSFORM_TEX(node_7106, _flowmap),0.0,0));
                v.vertex.xyz += (_flowmap_var.rgb*v.normal*_flowstrengh);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_9659 = _Time + _TimeEditor;
                float2 node_2124 = (i.uv0+(_rimflowspeed*node_9659.g)*float2(0,0.2));
                float4 node_4835 = tex2D(_rimtexture,TRANSFORM_TEX(node_2124, _rimtexture));
                float2 node_8681 = (i.uv0+(_rimflowspeed*node_9659.g)*float2(0.2,0));
                float4 node_4867 = tex2D(_rimtexture,TRANSFORM_TEX(node_8681, _rimtexture));
                float4 _masktexture_var = tex2D(_masktexture,TRANSFORM_TEX(i.uv0, _masktexture));
                float3 emissive = ((_rimcolor.rgb*(node_4835.rgb*node_4867.rgb))*(_masktexture_var.rgb.rgb*_rimstrengh));
                float4 _texture_var = tex2D(_texture,TRANSFORM_TEX(i.uv0, _texture));
                float3 finalColor = emissive + (_mainccolor.rgb*_texture_var.rgb);
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform float4 _TimeEditor;
            uniform sampler2D _flowmap; uniform float4 _flowmap_ST;
            uniform float _flowspeed;
            uniform float _flowstrengh;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_9659 = _Time + _TimeEditor;
                float2 node_7106 = ((o.uv0+(node_9659.g*_flowspeed)*float2(1,1))*1.0+0.0);
                float4 _flowmap_var = tex2Dlod(_flowmap,float4(TRANSFORM_TEX(node_7106, _flowmap),0.0,0));
                v.vertex.xyz += (_flowmap_var.rgb*v.normal*_flowstrengh);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
